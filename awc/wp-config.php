<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'awc');


/** MySQL database username */
define('DB_USER', 'awc');


/** MySQL database password */
define('DB_PASSWORD', 'awc@awc');


/** MySQL hostname */
define('DB_HOST', 'localhost');


/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');


/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[Xo:B*aD[NS,f3qsA6jZ.; lV6+b)h />fjDBBJz}J8Mh,6{zMy&w[9e?UagY!n4');

define('SECURE_AUTH_KEY',  'Kt7 #HF]sLws`0.yIiL~#MNKX/$6G*[*qzw?qSfR{G;ZY>5gi-rY#1p|]ZD]y;::');

define('LOGGED_IN_KEY',    '+=6UQih}(0$vP3%0kjelX*]U5SPpqC>h>4T,ASy?KHqvz1`5N;9w)4<B5VtUO5S[');

define('NONCE_KEY',        'n%y`,*.ydI`*3>z9&60epMTz|T2=Ke),LD3$1Hun;e!_t{(~:Rfo[>lF>d/9+WCI');

define('AUTH_SALT',        '^0&Dd~naO}Aq|_woJ9Ku>;0)*SG^En$%nMa:UC3v^.^EJr]h !$uCb;hu=s(Q5T=');

define('SECURE_AUTH_SALT', ',eS406FC~sN4pZL*}E]W7hSn_gA0O>_h9pby!tcWs,F~qv8u JN] mm!b3?qw:eD');

define('LOGGED_IN_SALT',   '7ll[.cts$vOur[Oi#9mf lP)!-N(EgD2^[J`$S eY)/_Yiac-tI9PMi!Q.]kwJrM');

define('NONCE_SALT',       '/+,ce=d7<3efUVHqLb4NdP15ZGzn.A=Ny3v%~-^anZs-IOl+I%e,z$by^/D=paQ ');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define ('WPLANG', 'en_GB');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
