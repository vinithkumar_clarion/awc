
<?php

    if (is_array($tweets)) {



// to use with intents

        //echo '<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>';


        $i = 1;
        foreach ($tweets as $tweet) {

            //$this->print_array($tweet);

            ?>

          <?php if($i % 2 == 1) { ?>
          <div class="item <?php if($i == 1) { ?> active <?php } ?>">
                <?php } ?>

             <div class="row">



                        <?php

                        if ($tweet->text) {

                            $the_tweet = ' '.$tweet->text . ' '; //adding an extra space to convert hast tag into links

                            $the_tweet = $this->makeClickableLinks($the_tweet);



                            // i. User_mentions must link to the mentioned user's profile.

                            if (is_array($tweet->entities->user_mentions))
                            {

                                foreach ($tweet->entities->user_mentions as $key => $user_mention)
                                {

                                    $the_tweet = preg_replace(

                                            '/@' . $user_mention->screen_name . '/i', '<a href="http://www.twitter.com/' . $user_mention->screen_name . '" target="_blank">@' . $user_mention->screen_name . '</a>', $the_tweet);

                                }

                            }



                            // ii. Hashtags must link to a twitter.com search with the hashtag as the query.

                            if (is_array($tweet->entities->hashtags))
                             {

                                foreach ($tweet->entities->hashtags as $hashtag) {

                                    $the_tweet = str_replace(' #' . $hashtag->text . ' ', ' <a href="https://twitter.com/search?q=%23' . $hashtag->text . '&src=hash" target="_blank">#' . $hashtag->text . '</a> ', $the_tweet);

                                }

                            }



                            ?>
                    <div class="col-xs-2">
                                <a href="https://twitter.com/i/web/status/<?php echo $tweet->id_str;?>" target="_blank">
                                    <i class="fa fa-twitter fa-3x"></i>
                               </a>
                    </div>
                    <div class="col-xs-10">
                                <h3>A Wider Circle</h3>
                               <?php echo $the_tweet . ' '; ?>
                    </div> <!-- col-xs-10 closed -->

                    <?php if ($aptf_settings['display_username'] == 1) { ?><a href="http://twitter.com/<?php echo $username; ?>" class="aptf-tweet-name" target="_blank"><?php echo $username; ?></a> <?php } ?>


                        <?php

                    } else {

                        ?>



                        <p><a href="http://twitter.com/'<?php echo $username; ?> " target="_blank"><?php _e('Click here to read ' . $username . '\'S Twitter feed', 'accesspress-twitter-feed'); ?></a></p></a>

                        <?php

                    }

                    ?>
          </div> <!-- row closed -->
  <?php if( ($i % 2 == 0) || (count($tweets) / $i == 1 )) {  ?>
  </div> <!-- item closed -->
  <?php } ?>



            <?php
          $i++;
        }   // For loop end

    }

    ?>



<?php if(isset($aptf_settings['display_follow_button']) && $aptf_settings['display_follow_button']==1){

    include(plugin_dir_path(__FILE__) . '../follow-btn.php');

}

?>

