<?php
/*
 * Default Events List Template
 * This page displays a list of events, called during the em_content() if this is an events list page.
 * You can override the default display settings pages by copying this file to yourthemefolder/plugins/events-manager/templates/ and modifying it however you need.
 * You can display events however you wish, there are a few variables made available to you:
 * 
 * $args - the args passed onto EM_Events::output()
 *
 */

$args = apply_filters('em_content_events_args', $args);

if( get_option('dbem_css_evlist') ) { ?>

<div <?php if($args['sidebar'] == "left") { ?> id="left-Carousel" <?php } else { ?> id="myCarousel" <?php } ?> class="carousel fdi-Carousel slide" style="padding-top: 15px !important;">
     <!-- Carousel items -->
        <div class="carousel fdi-Carousel slide" data-interval="0">
<?php
echo '<div class="carousel-inner onebyone-carosel">';

        if($args['category'] != '')
        {
          $events = EM_Events::get(array('scope'=>'future','category'=>$args['category']));
          $cat_id = 2;
        }
        else if($args['event'] != '')
        {
          $events = EM_Events::get(array('scope'=>'future','post_id'=>$args['event']));
          $cat_id = 2;
        }
         else
        {
          $events = EM_Events::get(array('scope'=>'future'));
          $cat_id = 1;
        }
        $e = 1;


        foreach( $events as $EM_Event )
        {
            $month = date("M", strtotime($EM_Event->event_start_date));
            $year =  date("Y", strtotime($EM_Event->event_start_date));

    ?>
          <?php if( ($e % $args['limit'] == 1 && $args['limit'] > 1) || ($args['limit'] == 1)) { ?>
          <div class="item <?php if($e == 1) { ?>active<?php } ?>"> <?php } ?>
          <?php if($args['limit'] > 1) { ?>
                    <div <?php if(count($events) == 1 && $args['sidebar'] == '') { ?> class="col-sm-10" <?php } else { ?> class="col-sm-6" <?php } ?>> <?php } ?>
                        <div class="event-carousel">
                            <div class="event-carouselHeader"></div>
                            <div class="event-carouselContent">
                            <div class="event-carouselBadge"><?php echo $month; ?>
                                <span class="f22"><?php echo $year; ?></span></div>
                                <a href="<?php echo $EM_Event->output("#_EVENTURL"); ?>">
                                  <?php if($EM_Event->output("#_EVENTIMAGEURL") != '') { ?>
                                    <img src="<?php echo $EM_Event->output("#_EVENTIMAGEURL"); ?>" class="lefty">
                                  <?php } else { ?>
                                    <img src="<?php echo get_template_directory_uri();?>/img/no-image.png" class="lefty">
                                  <?php } ?>
                                    </a>

                                <h2><?php echo $EM_Event->output("#_EVENTLINK"); ?></h2>
                                <p><?php $string = $EM_Event->output("#_EVENTNOTES"); echo $string = mb_strimwidth($string, 0, 100, '...'); ?></p>
                            </div>
                        </div>
            <?php if($args['limit'] > 1) { ?>
                    </div> <?php } ?>
         <?php if( ($e % $args['limit'] == 0) || (count($events) / $e == 0 )) { ?>
         </div> <?php } ?>
    <?php $e++; } ?>
</div>
<?php if($args['sidebar'] == "left" && count($events) > 1) { ?>

     <a class="left carousel-control" href="#left-Carousel" data-slide="prev">
        <span class="arw-prev">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </span>
    </a>

    <a class="right carousel-control" href="#left-Carousel" data-slide="next">
        <span class="arw-next">
            <i class="fa fa-chevron-right"></i>
        </span>
    </a>
<?php } else if(count($events) > 2) { ?>
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="arw-prev">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </span>
    </a>

    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="arw-next">
            <i class="fa fa-chevron-right"></i>
        </span>
    </a>
 <?php } ?>
</div>
<!--/carousel-inner-->
</div><!--/myCarousel-->
<?php } ?>