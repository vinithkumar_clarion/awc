<?php
require_once('../../../wp-load.php');
?>
<div class="col-sm-4">
        <h4>INSTAGRAM</h4>
        <div class="row">
        <?php echo do_shortcode( '[fts_instagram instagram_id=1129161639 pics_count=4 type=user]' ); ?>
        </div>
</div>
<script src="<?php bloginfo('template_url') ?>/js/jquery-2.1.3.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/responsive-tabs.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/responsive.js"></script>