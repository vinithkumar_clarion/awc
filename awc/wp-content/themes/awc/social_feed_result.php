<?php
require_once('../../../wp-load.php');
?>
<div class="social-feed">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2>SOCIAL MEDIA FEED</h2>

                    <div class="col-sm-4">
                            <h4>FACEBOOK</h4>
                            <div id="myCarousel-facebook" class="vertical-slider carousel vertical slide col-md-12" data-ride="carousel">
                                   <!-- Carousel items -->
                                    <div class="carousel-inner">
                                    <?php echo do_shortcode( '[facebook-feed page_id="awidercircle" post_limit="2" char_limit="100"]'); ?>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <span data-slide="prev" class="btn-vertical-facebook">
                                                See More
                                                <span class="block"><i class="fa fa-2x fa-angle-down"></i></span>
                                            </span>
                                        </div>
                                    </div>
                                    </div>
                    </div>

                    <div class="col-sm-4">
                            <h4>Twitter</h4>
                            <div id="myCarousel-twitter" class="vertical-slider carousel vertical slide col-md-12" data-ride="carousel">
                                    <!-- Carousel items -->
                                    <div class="carousel-inner">
                                      <?php //echo do_shortcode( '[fts_twitter twitter_name=awidercircle tweets_count=2 show_retweets=no]'); ?>
                                      <?php echo do_shortcode( '[ap-twitter-feed template="template-1"]'); ?>
                                    </div>  <!-- Carousel closed -->

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <span data-slide="prev" class="btn-vertical-twitter">
                                                See More
                                                <span class="block"><i class="fa fa-2x fa-angle-down"></i></span>
                                            </span>
                                        </div>  <!-- col-sm-12 closed -->
                                    </div>  <!-- row closed -->
                            </div><!-- myCarousel twitter closed -->
                    </div>

                    <div class="col-sm-4">
                            <h4>INSTAGRAM</h4>
                            <div class="row">
                            <?php echo do_shortcode( '[fts_instagram instagram_id=1129161639 pics_count=4 type=user]' ); ?>
                            </div>
                    </div>

                  </div>
                </div>
            </div>
       </div>
<script src="<?php bloginfo('template_url') ?>/js/jquery-2.1.3.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/responsive-tabs.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/responsive.js"></script>
