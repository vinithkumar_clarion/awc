<?php
require_once('../../../wp-load.php');
?>
<div class="col-sm-4">
        <h4>Twitter</h4>
        <div id="myCarousel-twitter" class="vertical-slider carousel vertical slide col-md-12" data-ride="carousel">
                <!-- Carousel items -->
                <div class="carousel-inner">
                  <?php //echo do_shortcode( '[fts_twitter twitter_name=awidercircle tweets_count=2 show_retweets=no]'); ?>
                  <?php echo do_shortcode( '[ap-twitter-feed template="template-1"]'); ?>
                </div>  <!-- Carousel closed -->

                <div class="row">
                    <div class="col-sm-12">
                        <span data-slide="prev" class="btn-vertical-twitter">
                            See More
                            <span class="block"><i class="fa fa-2x fa-angle-down"></i></span>
                        </span>
                    </div>  <!-- col-sm-12 closed -->
                </div>  <!-- row closed -->
        </div><!-- myCarousel twitter closed -->
</div>
<script src="<?php bloginfo('template_url') ?>/js/jquery-2.1.3.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/responsive-tabs.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/responsive.js"></script>