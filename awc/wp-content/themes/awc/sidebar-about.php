             <!-- Left part Start -->
                <div class="col-sm-4">
                    <?php $page_details = get_page(); $page_details->post_title; ?>
                    <!--nav start-->
                        <div class="left-nav">
                            <div class="nav-menu-header"><?php echo "About";?></div>
                            <?php
                                        $args = array(
                                        'menu'    => 'Header',
                                        'submenu' => 'About',
                                        'fallback_cb'    => false
                                        );

                                        wp_nav_menu( $args );
                            ?>
                        </div>
                    <!--nav end-->



            <!-- carousal start-->
                <div class="left-carousel">
                            <?php $events_short_code = get_field('events_short_code');
                            if($events_short_code != '')
                            {
                               echo do_shortcode( ''.$events_short_code.'' );
                            } else {
                               echo do_shortcode( '[events_list limit=1 sidebar="left"]' );
                            } ?>
                </div>
            <!-- carousal end-->
        <?php

$side_menu_banner_query="select wp.guid,wp.post_content,wp.post_excerpt from wp_posts AS wp
inner join wp_term_relationships AS wtr ON wtr.object_id = wp.ID
inner join wp_terms AS wt ON wt.term_id = wtr.term_taxonomy_id
where wt.name = 'Side Bar' AND wp.post_type = 'attachment' ORDER BY wp.ID DESC LIMIT 1";
$wpdb->query($side_menu_banner_query);
$side_menu_banner_details = $wpdb->get_row($side_menu_banner_query);
        ?>
            <div class="left-box">
                     <img src="<?php echo $side_menu_banner_details->guid; ?>" class="responsive-img" alt="Img12">
                 <p><?php echo $side_menu_banner_details->post_content; ?></p>
              </div>
              <?php if ( is_active_sidebar( 'sidebar-1' )  ) : ?>
<aside id="secondary" class="sidebar widget-area" role="complementary">
<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside><!-- .sidebar .widget-area -->
<?php endif; ?>
             <!-- Left part End -->
            </div>
