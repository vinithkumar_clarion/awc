<?php
/*
Template Name: Custom WordPress Event Page
*/
global $wpdb;
get_header();

?>
       <!-- Content part Start -->
        <div class="container">
            <div class="inner-wrap">
                <div class="row">

                <?php get_template_part('sidebar', 'awc'); ?>
                <?php
                $id = get_the_ID();
                $res = get_page();
                ?>


                <!-- middle part Start -->
                 <div class="col-sm-8">
                <div class="middle">

                    <h2><?php echo do_shortcode( '[event post_id="'.$id.'"]#_EVENTNAME[/event] ' ); ?></h2>

                    <div class="right-box">
                    <?php global $EM_Event;  ?>
                    <p><?php if($EM_Event->output("#_EVENTIMAGEURL") != '') { ?>
                        <img src="<?php echo $EM_Event->output("#_EVENTIMAGEURL"); ?>" class="lefty">
                    <?php } else { ?>
                        <img src="<?php echo get_template_directory_uri();?>/img/no-image.png" class="lefty">
                    <?php } ?>
                    </p>
                    <p><?php //echo do_shortcode( '[event post_id="'.$id.'"]DATE :- #_EVENTDATES at #_EVENTTIMES[/event] ' );
/* @var $EM_Event EM_Event */

echo $EM_Event->output_single();
                    ?></p>
                    </div>
                </div>
                </div>
             <!-- middle part End -->


             </div>
            </div>
        </div>
       <!-- Content part End -->

            <div class="clearfix"></div>

        <!-- News Letter Part Start -->
        <div class="news-letter volunteer">
            <div class="container">
                <div class="row">
                   <div class="col-sm-12">
                        <h2>VOLUNTEER WITH US</h2>
                            <p>Want to become part of our volunteer family? Sign up here and we’ll be in touch!</p>
                 </div>
                         <div class="col-sm-12">
                         <?php echo do_shortcode( '[ninja_form id=1]' ) ?>
                         </div>

                </div>
            </div>
        </div>
       <!-- News Letter Part End -->
       <div class="clearfix"></div>
<?php

    get_footer();

    ?>
