<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Awc
 * @since awc 1.0
 */
 global $wpdb;
 $page_details = get_page();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>AWC - <?php echo $page_details->post_title; ?></title>
        <meta name="description" content="">
        <meta name="keywords" content="stage lighting, dj lighting, dance lights, uplighting, dj equipment, dj gear, pro audio, speakers, kpodj, kpodj.com, powered speakers, dj mixer, dj packages, dj system, dj store, wholesale dj equipment, cheap, discount, blizzard lighting, chauvet lighting, american dj, scrim king, global truss">
        <meta http-equiv="Content-Type" content="text/xhtml; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>


        <!-- Bootstrap core CSS -->
        <link href="<?php bloginfo('template_url') ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Font Awasome Icon -->
        <link href="<?php bloginfo('template_url') ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <!-- Google Lato and EkMukta Fonts-->
        <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,100italic,300italic,400italic,700italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <!-- Custom styles for this template -->
        <link href="<?php bloginfo('template_url') ?>/css/common.css" rel="stylesheet" type="text/css" />
        <link href="<?php bloginfo('template_url') ?>/css/style.css" rel="stylesheet" type="text/css" />

        <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url') ?>/js/html5shiv.js"></script>
        <script src="<?php bloginfo('template_url') ?>/js/respond.min.js"></script>
        <![endif]-->

        <script src="<?php bloginfo('template_url') ?>/js/jquery-2.1.3.min.js"></script>
        <!--<script src="<?php bloginfo('template_url') ?>/js/bootstrap.min.js"></script>
        <script src="<?php bloginfo('template_url') ?>/js/responsive-tabs.js"></script>
        <script src="<?php bloginfo('template_url') ?>/js/responsive.js"></script>-->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>


    </head>
    <body>
        <!-- top bar part start -->
            <header>
                <div class="top-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                               <?php  echo wp_nav_menu( array('menu' => 'Promo Text'));?>
                            </div>

                            <div class="col-sm-6">
                               <div class="nav navbar-right top-nav">
                                    <?php
                                           echo wp_nav_menu( array('menu' => 'Header Top'));
                                     ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        <!-- top bar part end -->

        <!--main menu part start -->
                <nav class="main-menu navbar navbar-default">
                    <div class="container">
                      <div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </button>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo">
                        <img src="<?php echo get_template_directory_uri();?>/img/logo.png" alt="logo"/></a>
                      </div>
                      <div class="navbar-collapse collapse" id="navbar">
                            <?php
                                echo str_replace('sub-menu', 'dropdown-menu', wp_nav_menu( array(
                                'echo' => false,
                                'menu' => 'Header',
                                'menu_class' => 'nav navbar-nav',
                                'walker'          => new Primary_Walker_Nav_Menu()
                                ) )
                                );
                            ?>
                      <!-- <ul class="nav navbar-nav navbar-right donate">
                          <li>
                          <?php echo do_shortcode('[maxbutton id="1"]'); ?>
                          </li>
                       </ul> -->
                      </div><!--/.nav-collapse -->
                    </div><!--/.container-fluid -->
                  </nav>
        <!--main menu part End -->
       </header>
        <?php echo do_shortcode("[huge_it_slider id='2']"); ?>

        <!-- Banner part Start -->
           <!-- <div id="myCarousel" class="carousel slide" data-interval="3000" data-ride="carousel">

                <ol class="carousel-indicators">
                <?php $mc=0; foreach($home_banner_details as $results) { ?>
                    <li data-target="#myCarousel" data-slide-to="<?php echo $mc;?>" <?php if($mc == 0) { ?> class="active" <?php } ?>></li>
                <?php $mc++; } ?>
                </ol>

                <div class="carousel-inner">
                   <?php $k=0; foreach($home_banner_details as $results) { ?>
                   <div class="<?php if($k == 0) { ?> active <?php } ?> item">
                        <img src="<?php echo $results->guid; ?>" alt="First Slide"/>
                        <div class="carousel-caption">
                          <h1><?php echo $results->post_excerpt; ?></h1>
                          <p><?php echo $results->post_content; ?></p>
                        </div>
                   </div>
                 <?php $k++; } ?>

                </div>


            </div> -->
    <!-- Banner part End -->