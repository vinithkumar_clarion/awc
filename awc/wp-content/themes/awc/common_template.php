<?php
/*
Template Name: Custom WordPress Template Page
*/
global $wpdb;
get_header();
?>
       <!-- Content part Start -->
        <div class="container">
            <div class="inner-wrap">
                <div class="row">
                        <?php  get_template_part('sidebar', 'awc');
                        $id = get_the_ID();  $res = get_page(); ?>
                <!-- middle part Start -->
                 <div class="col-sm-8">
                          <div class="middle">

                                 <?php
        // Start the loop.
        while ( have_posts() ) : the_post();

            // Include the page content template.
            get_template_part( 'template-parts/content-custom', 'page' );

            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) {
                comments_template();
            }

            // End of the loop.
        endwhile;
        ?>
                            </div>
                    </div>
                 <!-- middle part End -->
             </div>
            </div>
        </div>
       <!-- Content part End -->

            <div class="clearfix"></div>

        <!-- News Letter Part Start -->
        <div class="news-letter volunteer">
            <div class="container">
                <div class="row">
                   <div class="col-sm-12">
                        <h2>VOLUNTEER WITH US</h2>
                            <p>Want to become part of our volunteer family? Sign up here and we’ll be in touch!</p>
                 </div>
                         <div class="col-sm-12">
                         <?php echo do_shortcode( '[ninja_form id=1]' ) ?>

                         </div>

                </div>
            </div>
        </div>
       <!-- News Letter Part End -->
       <div class="clearfix"></div>
<?php get_footer();  ?>
