<?php
/**
 * The template part for displaying custom content
 *
 * @package WordPress
 * @subpackage Awc
 * @since AWC 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
			<span class="sticky-post"><?php _e( 'Featured', 'awc' ); ?></span>
		<?php endif; ?>

		<?php the_title( sprintf( '<h2 class="entry-title">', esc_url( get_permalink() ) ), '</h2>' ); ?>
	</header><!-- .entry-header -->
    <?php
/*
* Paginate Advanced Custom Field repeater
*/

if( get_query_var('paged') ) {
$page = get_query_var( 'paged' );
} else {
$page = 1;
}

if( get_query_var('paged') ) {
$page1 = get_query_var( 'paged' );
} else {
$page1 = 1;
}

// Variables
$row              = 0;
$images_per_page  = 5; // How many images to display on each page
$images           = get_field( 'staff' );
$total            = count( $images );
$pages            = ceil( $total / $images_per_page );
$min              = ( ( $page * $images_per_page ) - $images_per_page ) + 1;
$max              = ( $min + $images_per_page ) - 1;

$row1              = 0;
$images_per_page1  = 5; // How many images to display on each page
$images1           = get_field( 'board' );
$total1            = count( $images1 );
$pages1            = ceil( $total1 / $images_per_page1 );
$min1              = ( ( $page1 * $images_per_page1 ) - $images_per_page1 ) + 1;
$max1              = ( $min1 + $images_per_page1 ) - 1;

?>
    <?php if (get_field('staff')) {  $i = 1;
             while (has_sub_field('staff')) { //the_row();
             $row++;

// Ignore this image if $row is lower than $min
if($row < $min) { continue; }

// Stop loop completely if $row is higher than $max
if($row > $max) { break; }
        ?>
    <!--acordion start-->
                <div class="accordion-wrap <?php if($i == 1) { ?> open<?php } ?>">
                    <div class="accord">
                         <span class="accord-img">
                              <img src="<?php the_sub_field('staff_profile_image'); ?>" alt="img 14">
                         </span>
                       <span class="bold"> <?php the_sub_field('staff_name'); ?>, </span> <?php the_sub_field('staff_designation'); ?>
                      <span class="pull-right mt20"> <i class="fa fa-2x toogleArrow"></i> </span>
                    </div>

                     <div class="accord-content  clearfix">
                        <p><?php the_sub_field('staff_description'); ?></p>
                    </div>
                  </div>
    <!--acordion end-->
      <?php $i++; } ?>
      <div style="text-align: right;">
      <?php
$big = 999999999; // need an unlikely integer
// Pagination

echo paginate_links( array(
'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
'format' => '?page=%#%',
'current' => $page,
'total' => $pages,
'prev_text'    => __('<<<'),
'next_text'    => __('>>>'),
) );
?>  </div>
      <?php } ?>
   <?php if (get_field('board')) {   $j =1;
             while (has_sub_field('board')) {
         $row1++;

// Ignore this image if $row is lower than $min
if($row1 < $min1) { continue; }

// Stop loop completely if $row is higher than $max
if($row1 > $max1) { break; }
?>
    <!--acordion start-->
                <div class="accordion-wrap <?php if($j == 1) { ?> open<?php } ?>">
                    <div class="accord">
                         <span class="accord-img">
                              <img src="<?php the_sub_field('board_profile_image'); ?>" alt="img 14">
                         </span>
                       <span class="bold"> <?php the_sub_field('board_name'); ?>, </span> <?php the_sub_field('board_designation'); ?>
                      <span class="pull-right mt20"> <i class="fa fa-2x toogleArrow"></i> </span>
                    </div>

                     <div class="accord-content  clearfix">
                        <p><?php the_sub_field('board_description'); ?></p>
                    </div>
                  </div>
    <!--acordion end-->
      <?php $j++; } ?>
      <div style="text-align: right;">
      <?php
$big1 = 999999999; // need an unlikely integer
// Pagination

echo paginate_links( array(
'base' => str_replace( $big1, '%#%', esc_url( get_pagenum_link( $big1 ) ) ),
'format' => '?page=%#%',
'current' => $page1,
'total' => $pages1,
'prev_text'    => __('<<<'),
'next_text'    => __('>>>'),
) );
?>  </div>
      <?php } ?>
      <?php $res1 = get_page();  ?>
      <?php if($res1->post_type != 'news') { ?>
                    <!--testimonials start-->
                    <div class="testimonials" style="padding-top:20px !important;padding-bottom:20px !important;">
                        <?php echo do_shortcode('[testimonials category="'.$res1->post_name.'"]'); ?>
                    </div>
                    <!--testimonials end-->
      <?php } ?>
	<?php awc_excerpt(); ?>

	<?php //awc_post_thumbnail(); ?>
    <?php //if($res1->post_name == 'jobs') {  echo do_shortcode('[jobs]'); } ?>
	<div class="entry-content">
		<?php
        	/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'awc' ),
				get_the_title()
			) );

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'awc' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'awc' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
