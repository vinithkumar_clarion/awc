<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Awc
 * @since awc 1.0
 */
?>
<!-- footer part Start -->
         <footer>
             <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="img-responsive" src="<?php echo get_template_directory_uri();?>/img/footer-logo.png" alt="footer logo"/></a>
                        <ul class="footer-nav">
                          <li><a href="http://charityreports.bbb.org/public/seal.aspx?ID=179541662011" target="_blank"><img src="<?php echo get_template_directory_uri();?>/img/bbb.png" alt="bbc"/></a></li>
                          <li><a href="http://www.catalogueforphilanthropy-dc.org/cfpdc/index.php" target="_blank"><img src="<?php echo get_template_directory_uri();?>/img/philanthropy.png" alt="philanthropy"/></a></li>
                        </ul>
                    </div>

            <?php footer_text() ?>
            <?php echo wp_nav_menu( array('menu' => 'PRIVACY POLICY', 'walker' => new Footer_Walker_Nav_Menu())); ?>
             </div>
         </footer>
        <!-- footer part End -->

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <script src="<?php bloginfo('template_url') ?>/js/jquery-2.1.3.min.js"></script>
        <script src="<?php bloginfo('template_url') ?>/js/bootstrap.min.js"></script>
        <script src="<?php bloginfo('template_url') ?>/js/responsive-tabs.js"></script>
        <script src="<?php bloginfo('template_url') ?>/js/responsive.js"></script>
        <?php $res_foot = get_page(); if($res_foot->post_name == 'jobs') { ?>
            <script type='text/javascript' src='http://61.12.77.244/AWC/wp-content/plugins/wp-job-manager/assets/js/jquery-deserialize/jquery.deserialize.js?ver=1.2.1'></script>
            <script type='text/javascript'>
            /* <![CDATA[ */
            var job_manager_chosen_multiselect_args = {"search_contains":"1"};
            /* ]]> */
            </script>
            <script type='text/javascript' src='http://61.12.77.244/AWC/wp-content/plugins/wp-job-manager/assets/js/jquery-chosen/chosen.jquery.min.js?ver=1.1.0'></script>
            <script type='text/javascript'>
            /* <![CDATA[ */
            var job_manager_ajax_filters = {"ajax_url":"\/AWC\/jm-ajax\/%%endpoint%%\/","is_rtl":"0","i18n_load_prev_listings":"Load previous listings"};
            /* ]]> */
            </script>
            <script type='text/javascript' src='http://61.12.77.244/AWC/wp-content/plugins/wp-job-manager/assets/js/ajax-filters.min.js?ver=1.25.0'></script>
        <?php } ?>
    <?php wp_footer(); ?>
    </body>

    </html>