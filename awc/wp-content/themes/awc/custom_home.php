<?php
/*
Template Name: Custom WordPress Home Page
*/
global $wpdb;
get_header('home');

$args_main = array(
  'numberposts' => 4,
  'post_type'   => 'post',
  'category'   => get_cat_ID('Home')
);
$get_home_posts = get_posts( $args_main );

$args = array(
  'numberposts' => 4,
  'post_type'   => 'post',
  'category'   => get_cat_ID('Home-Accordion')
);
$get_home_accordion_posts = get_posts( $args );

?>
<style>.date { display : none;} .heart-comments-wrap { display : none;} </style>
 <!-- Intro part Start -->
        <div class="intro-wrap">
            <div>
                <div>
                      <div class="col-md-12 intro-text">
                        <h2 class="text-center"><?php echo $get_home_posts[0]->post_title; ?></h2>
                        <p><?php echo nl2br($get_home_posts[0]->post_content); ?></p>
                    </div>
                </div>
          </div>
        </div>
        <!-- Intro part End -->

    <div class="clearfix"></div>
        <!-- Responsive Tab and Accordion Part Start -->
            <div class="tab-acordion">
            <div class="container">
                <h2>you can help using these four programs</h2>
                <ul id="myTab" class="nav nav-tabs responsive hidden-xs hidden-sm">
                    <?php $i=0; foreach($get_home_accordion_posts as $results) {
                                  $img_query="select guid from wp_posts where post_parent = '".$results->ID."' AND post_type = 'attachment' ORDER BY ID DESC LIMIT 0,1";
                                  $wpdb->query($img_query);
                                  $img_get_details = $wpdb->get_row($img_query);
                                  $status = get_post_meta($results->ID, 'active_accordion');
                        ?>
                     <li class="test-class col-sm-3 <?php if($status[0] == 'active') { ?> active <?php } ?>">
                            <a href="#resp-tab<?php echo $results->ID; ?>" class="deco-none">
                            <span class="tab-icon">
                                <img class="resposive-img" src="<?php echo $img_get_details->guid; ?>" alt="img02"/>
                            </span>
                            <span class="block"><?php echo $results->post_title; ?></span>
                            </a>
                    </li>
                    <?php $i++; } ?>
                 </ul>

               <div class="tab-content responsive hidden-xs hidden-sm">

                    <!-- First Tab Start -->
                   <?php $j=0; foreach($get_home_accordion_posts as $res) { ?>
                    <div id="resp-tab<?php echo $res->ID; ?>" class="tab-pane <?php if($j == 3) { ?>active<?php } ?>">
                        <?php //$content = str_replace('<em>', '', $res->post_content);
                              //$content = str_replace('</em>', '', $content); echo $content;
                              echo $res->post_content; ?>
                    </div>
                    <?php $j++; } ?>

                    <!-- First Tab End -->
      </div>

            </div>
         </div>
        <!-- Responsive Tab and Accordion Part End -->
            <div class="clearfix"></div>

        <!-- News Letter Part Start -->
        <div class="news-letter">
            <div class="container">
                <div class="row">
                   <div class="col-sm-12">

                         <div class="col-sm-4">
                    <h2>BUILD YOUR COMMUNITY</h2>
                    <p>Join our newsletter to learn more!</p>
                 </div>

                         <div class="col-sm-7" id="newsletter_form">
                                    <div class="input-prepend">
                                        <input type="text" class="news-input" placeholder="name@gmail.com" name="btn_news_text" id="btn_news_text">
                                        <span class="add-on"><button type="buttom" class="btn-news" style="padding: 28px 30px !important;" id="btn_news">SUBMIT</button></span>
                                    </div>
                        </div>
                        <div id="news-letter-warning" style="clear: both;float: right;padding-right: 400px;color: red;display:none;">Please enter valid email address</div>
                    </div>
                </div>
            </div>
        </div>
       <!-- News Letter Part End -->
       <div class="clearfix"></div>
       <!-- news slider part Start -->
       <div class="news-slider">
                <div class="container">
                     <div class="row">
                         <div class="col-sm-12">
                           <?php echo do_shortcode( '[events_list limit=2] ' ); ?>
                        </div>
                    </div>
             </div>
        </div>
       <!-- news slider part End -->

       <!-- Social feed part Start -->

       <div class="social-feed">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2>SOCIAL MEDIA FEED</h2>
                        <div id="show_feed_load"></div>
                          <div id="show_facbook_feed"></div>
                          <div id="show_twitter_feed"></div>
                          <div id="show_instagram_feed"></div>
                        </div>
                </div>
            </div>
       </div>
       <!-- Social feed part End -->
       <?php

    get_footer();

    ?>

    <script>
        $(document).ready(function()
        {
            doFacebookAjax();
            doTwitterAjax();
            doInstagramAjax();

            function doFacebookAjax()
            {
                $( "#show_feed_load" ).html("<img style='padding: 20px 0px 20px 210px;' src='<?php echo get_template_directory_uri();?>/img/lightbox-ico-loading.gif'/>");
                $.post( "<?php bloginfo('template_url') ?>/social_feed_facebook.php", function( data )
                {
                   $( "#show_facbook_feed" ).html( data );
                   $( "#show_feed_load" ).html( "" );
                });
            }
            function doTwitterAjax()
            {
                $( "#show_feed_load" ).html("<img style='padding: 20px 0px 20px 210px;' src='<?php echo get_template_directory_uri();?>/img/lightbox-ico-loading.gif'/>");
                $.post( "<?php bloginfo('template_url') ?>/social_feed_twitter.php", function( data )
                {
                   $( "#show_twitter_feed" ).html( data );
                });
            }
            function doInstagramAjax()
            {
                $( "#show_feed_load" ).html("<img style='padding: 20px 0px 20px 210px;' src='<?php echo get_template_directory_uri();?>/img/lightbox-ico-loading.gif'/>");
                $.post( "<?php bloginfo('template_url') ?>/social_feed_instagram.php", function( data )
                {
                   $( "#show_instagram_feed" ).html( data );
                });
            }
        });
       $("#btn_news").click(function()
       {
         var email_address = $("#btn_news_text").val();
         if(isValidEmailAddress(email_address))
         {
             $("#newsletter_form").html("<div style='padding-top: 58px;font-size: 16px;color: #101010;'>Thank you for Joining Our Newsletter.</div>");
             window.open('http://oi.vresp.com/?fid=5d911b8413&email_address='+email_address, '_blank');
             $("#news-letter-warning").hide();
         } else {
            $("#news-letter-warning").show();
         }
       });

       function isValidEmailAddress(emailAddress) {
                var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
                return pattern.test(emailAddress);
       }

    </script>
