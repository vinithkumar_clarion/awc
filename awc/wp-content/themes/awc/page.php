<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Awc
 * @since awc 1.0
 */

get_header(); ?>
       <!-- Content part Start -->
        <div class="container">
            <div class="inner-wrap">
                <div class="row">

                <?php get_template_part('sidebar', 'awc'); ?>
                <?php
                $id = get_the_ID();
                $res = get_page();
                ?>


                <!-- middle part Start -->
                 <div class="col-sm-8">
                <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();

            // Include the page content template.
            get_template_part( 'template-parts/content', 'page' );

            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) {
                comments_template();
            }

            // End of the loop.
        endwhile;
        ?>

    </main><!-- .site-main -->

    <?php get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->
                </div>
             <!-- middle part End -->


             </div>
            </div>
        </div>
       <!-- Content part End -->

            <div class="clearfix"></div>

        <!-- News Letter Part Start -->
        <div class="news-letter volunteer">
            <div class="container">
                <div class="row">
                   <div class="col-sm-12">
                        <h2>VOLUNTEER WITH US</h2>
                            <p>Want to become part of our volunteer family? Sign up here and we’ll be in touch!</p>
                 </div>
                         <div class="col-sm-12">
                         <?php echo do_shortcode( '[ninja_form id=1]' ) ?>
                         </div>

                </div>
            </div>
        </div>
       <!-- News Letter Part End -->
       <div class="clearfix"></div>
<?php

    get_footer();

    ?>
