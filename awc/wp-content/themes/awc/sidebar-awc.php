             <!-- Left part Start -->
                <div class="col-sm-4">
                                <?php $page_details = get_page();
                                if($page_details->post_type == 'event') {
                                ?>
                                    <style>
                                    #menu-item-473
                                    {
                                      border-left:5px solid #62B5E5;
                                    }
                                    </style>
                                <?php } else if($page_details->post_type == 'blog_post') { ?>
                                     <style>
                                    #menu-item-450
                                    {
                                      border-left:5px solid #62B5E5;
                                    }
                                    </style>
                                <?php } else if($page_details->post_type == 'news') { ?>
                                     <style>
                                    #menu-item-173
                                    {
                                      border-left:5px solid #62B5E5;
                                    }
                                    </style>
                                <?php } ?>
                    <?php
                                        if ( is_page() && $page_details->post_parent > 0 )
                                        {
                                            $args = array(
                                            'menu'    => 'Header',
                                            'submenu' => get_the_title( $page_details->post_parent ),
                                            'fallback_cb'    => false
                                            );

                                            $title = get_the_title( $page_details->post_parent );
                                        } else {
                                            $page_details_title = $page_details->post_title;
                                            $title = $page_details->post_title;
                                            if($page_details->post_type == "blog_post") { $page_details_title = $title= 'Ending Poverty'; }
                                            if($page_details->post_type == "event") { $page_details_title = $title= 'Get Involved'; }
                                            if($page_details->post_type == "news") { $page_details_title = $title= 'About'; }
                                            $args = array(
                                            'menu'    => 'Header',
                                            'submenu' => $page_details_title,
                                            'fallback_cb'    => false
                                            );

                                        }
                                        $submenu = wp_list_pages(
                                        array(
                                        'child_of' => $page_details->ID,
                                        'echo'     => false,
                                        )
                                        );
                             if( ($submenu) || (is_page() && $page_details->post_parent) || $page_details->post_type == "blog_post" || $page_details->post_type == "event" || $page_details->post_type == "news") {

                            ?>
                 <!--nav start-->
                        <div class="left-nav" style="margin-top: 0px !important;">
                            <div class="nav-menu-header"><?php echo $title;?></div>
                              <?php wp_nav_menu( $args );  ?>
                        </div>
                    <!--nav end-->
                    <?php } ?>
            <!-- carousal start-->
                <div class="left-carousel">
                           <?php $events_short_code = get_field('events_short_code');
                            if($events_short_code != '')
                            {
                               echo do_shortcode( ''.$events_short_code.'' );
                            } else {
                               echo do_shortcode( '[events_list limit=1 sidebar="left"]' );
                            } ?>
                 </div>
            <!-- carousal end-->
        <?php

$side_menu_banner_query="select wp.guid,wp.post_content,wp.post_excerpt from wp_posts AS wp
inner join wp_term_relationships AS wtr ON wtr.object_id = wp.ID
inner join wp_terms AS wt ON wt.term_id = wtr.term_taxonomy_id
where wt.name = 'Side Bar' AND wp.post_type = 'attachment' ORDER BY wp.ID DESC LIMIT 1";
$wpdb->query($side_menu_banner_query);
$side_menu_banner_details = $wpdb->get_row($side_menu_banner_query);
        ?>
            <div class="left-box">
                     <img src="<?php echo $side_menu_banner_details->guid; ?>" class="responsive-img" alt="Img12">
                 <p><?php echo $side_menu_banner_details->post_content; ?></p>
              </div>
<?php if ( is_active_sidebar( 'sidebar-1' )  ) : ?>
<aside id="secondary" class="sidebar widget-area" role="complementary">
<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside><!-- .sidebar .widget-area -->
<?php endif; ?>
             <!-- Left part End -->
            </div>

