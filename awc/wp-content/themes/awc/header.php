<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Awc
 * @since awc 1.0
 */
 global $wpdb;

$page_details = get_page();

$thumbnail_id = get_post_thumbnail_id(get_the_ID());
$thumbnail_details = get_posts(array('p' => $thumbnail_id, 'post_type' => 'attachment'));
$thumbnail_src = wp_get_attachment_image_src( $thumbnail_id, 'full' );

?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>AWC - <?php echo $page_details->post_title; ?></title>
        <meta name="description" content="">
        <meta name="keywords" content="stage lighting, dj lighting, dance lights, uplighting, dj equipment, dj gear, pro audio, speakers, kpodj, kpodj.com, powered speakers, dj mixer, dj packages, dj system, dj store, wholesale dj equipment, cheap, discount, blizzard lighting, chauvet lighting, american dj, scrim king, global truss">
        <meta http-equiv="Content-Type" content="text/xhtml; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>


        <!-- Bootstrap core CSS -->
        <link href="<?php bloginfo('template_url') ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Font Awasome Icon -->
        <link href="<?php bloginfo('template_url') ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <!-- Google Lato and EkMukta Fonts-->
        <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,100italic,300italic,400italic,700italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <!-- Custom styles for this template -->
        <link href="<?php bloginfo('template_url') ?>/css/common.css" rel="stylesheet" type="text/css" />
        <link href="<?php bloginfo('template_url') ?>/css/style.css" rel="stylesheet" type="text/css" />

        <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url') ?>/js/html5shiv.js"></script>
        <script src="<?php bloginfo('template_url') ?>/js/respond.min.js"></script>
        <![endif]-->

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <?php $plugin_url = plugins_url(); $res_head = get_page(); if($res_head->post_name == 'jobs') { ?>
        <link rel='stylesheet' id='chosen-css'  href='<?php echo $plugin_url;?>/wp-job-manager/assets/css/chosen.css?ver=4.6.1' type='text/css' media='all' />
        <link rel='stylesheet' id='wp-job-manager-frontend-css'  href='<?php echo $plugin_url;?>/wp-job-manager/assets/css/frontend.css?ver=4.6.1' type='text/css' media='all' />
        <?php } ?>
        <?php if($res_head->post_type == 'event') { ?>
        <link rel='stylesheet' id='events-manager-css'  href='<?php echo $plugin_url;?>/events-manager/includes/css/events_manager.css?ver=5.6624' type='text/css' media='all' />
        <script src="<?php bloginfo('template_url') ?>/js/jquery-2.1.3.min.js"></script>
        <script type='text/javascript'> 
/* <![CDATA[ */
var EM = {"ajaxurl":"<?php echo admin_url('admin-ajax.php'); ?>","locationajaxurl":"<?php echo admin_url('admin-ajax.php'); ?>?action=locations_search","firstDay":"1","locale":"en","dateFormat":"dd\/mm\/yy","ui_css":"<?php echo $plugin_url; ?>/events-manager\/includes\/css\/jquery-ui.min.css","show24hours":"0","is_ssl":"","bookingInProgress":"Please wait while the booking is being submitted.","tickets_save":"Save Ticket","bookingajaxurl":"http:\/\/61.12.77.244\/AWC\/wp-admin\/admin-ajax.php","bookings_export_save":"Export Bookings","bookings_settings_save":"Save Settings","booking_delete":"Are you sure you want to delete?","bb_full":"Sold Out","bb_book":"Book Now","bb_booking":"Booking...","bb_booked":"Booking Submitted","bb_error":"Booking Error. Try again?","bb_cancel":"Cancel","bb_canceling":"Canceling...","bb_cancelled":"Cancelled","bb_cancel_error":"Cancellation Error. Try again?","txt_search":"Search","txt_searching":"Searching...","txt_loading":"Loading...","event_reschedule_warning":"Are you sure you want to reschedule this recurring event? If you do this, you will lose all booking information and the old recurring events will be deleted.","event_detach_warning":"Are you sure you want to detach this event? By doing so, this event will be independent of the recurring set of events.","delete_recurrence_warning":"Are you sure you want to delete all recurrences of this event? All events will be moved to trash.","disable_bookings_warning":"Are you sure you want to disable bookings? If you do this and save, you will lose all previous bookings. If you wish to prevent further bookings, reduce the number of spaces available to the amount of bookings you currently have","booking_warning_cancel":"Are you sure you want to cancel your booking?"};
/* ]]> */
</script>
        <script type='text/javascript' src='<?php echo $plugin_url;?>/events-manager/includes/js/events-manager.js?ver=5.6624'></script>
        <?php } ?>
    </head>
    <body>
        <!-- top bar part start -->
            <header>
                <div class="top-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                               <?php  echo wp_nav_menu( array('menu' => 'Promo Text'));?>
                            </div>

                            <div class="col-sm-6">
                               <div class="nav navbar-right top-nav">
                                    <?php
                                           echo wp_nav_menu( array('menu' => 'Header Top'));
                                     ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        <!-- top bar part end -->

        <!--main menu part start -->
                <nav class="main-menu navbar navbar-default">
                    <div class="container">
                      <div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </button>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo">
                        <img src="<?php echo get_template_directory_uri();?>/img/logo.png" alt="logo"/></a>
                      </div>
                      <div class="navbar-collapse collapse" id="navbar">
                            <?php
                                echo str_replace('sub-menu', 'dropdown-menu', wp_nav_menu( array(
                                'echo' => false,
                                'menu' => 'Header',
                                'menu_class' => 'nav navbar-nav',
                                'walker'          => new Primary_Walker_Nav_Menu()
                                ) )
                                );
                            ?>
                      </div><!--/.nav-collapse -->
                    </div><!--/.container-fluid -->
                  </nav>
        <!--main menu part End -->
       </header>
       <?php if ($thumbnail_src && isset($thumbnail_src[0]) && $page_details->post_type != 'event') { ?>
        <!-- Banner part Start -->
            <div id="myCarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
                <!-- Carousel indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                <!-- Wrapper for carousel items -->
                <div class="carousel-inner">

                   <div class="active item">
                        <img src="<?php echo $thumbnail_src[0]; ?>" alt="First Slide"/>
                        <div class="carousel-caption">
                          <h1><?php echo $thumbnail_details[0]->post_excerpt; ?></h1>
                          <p><?php echo $thumbnail_details[0]->post_content; ?></p>
                        </div>
                   </div>
                </div>
                <!-- Carousel controls -->

            </div>
    <!-- Banner part End -->
    <?php } ?>