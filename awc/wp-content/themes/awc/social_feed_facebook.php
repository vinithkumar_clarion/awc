<?php
require_once('../../../wp-load.php');
?>
<div class="col-sm-4">
    <h4>FACEBOOK</h4>
    <div id="myCarousel-facebook" class="vertical-slider carousel vertical slide col-md-12" data-ride="carousel">
           <!-- Carousel items -->
            <div class="carousel-inner">
            <?php echo do_shortcode( '[facebook-feed page_id="awidercircle" post_limit="2" char_limit="100"]'); ?>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <span data-slide="prev" class="btn-vertical-facebook">
                        See More
                        <span class="block"><i class="fa fa-2x fa-angle-down"></i></span>
                    </span>
                </div>
            </div>
            </div>
</div>
