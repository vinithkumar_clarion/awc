$(document).ready(function() {

    //Stop Auto Carousel
    $('#myCarousel-twitter, #myCarousel-facebook').carousel({
        pause: true,
        interval: false
    });
    
    //Menu Clickable
        $('.main-menu li.dropdown > a:not(a[href="#"])').on('click', function() {
            self.location = $(this).attr('href');
    });

    //Mobile Dropdown
    $('.mb-dropdown').on('click', function(event) {
        event.preventDefault(); 
        event.stopPropagation(); 
        $(this).parent().siblings().find( ".fa" ).removeClass('fa-caret-down');
        $(this).parent().siblings().find( ".fa" ).addClass('fa-caret-right');
        $(this).find( ".fa" ).toggleClass('fa-caret-right');
        $(this).find( ".fa" ).toggleClass('fa-caret-down');
        $(this).parent().siblings().removeClass('open');
        $(this).parent().toggleClass('open');
    });
    
    //Click Outside of Menu  
        $(document).click(function (e) {
           $(this).find('#navbar').removeClass('in');
            var clickover = $(e.target);
            var $navbar = $(".navbar-collapse");               
            var _opened = $navbar.hasClass("in");
            if (_opened === true && !clickover.hasClass("navbar-toggle") ) {      
                $navbar.collapse('hide');
                //$('.navbar-collapse.collapse.in').css("display", "none !important");
            }
        });
    
    //Tab with Accourdion
    $( 'ul.nav.nav-tabs  a' ).click( function ( e ) {
        e.preventDefault();
        $( this ).tab( 'show' );
      });
      
      fakewaffle.responsiveTabs(['xs', 'sm']);
    
    //Social Feed  
    $('.btn-vertical-facebook').on('click', function () {
        if ($(this).attr('data-slide') == 'next') {
            $('#myCarousel-facebook').carousel('next');
        }
        if ($(this).attr('data-slide') == 'prev') {
            $('#myCarousel-facebook').carousel('prev')
        }
    });
    
    $('.btn-vertical-twitter').on('click', function () {
        if ($(this).attr('data-slide') == 'next') {
            $('#myCarousel-twitter').carousel('next');
        }
        if ($(this).attr('data-slide') == 'prev') {
            $('#myCarousel-twitter').carousel('prev')
        }
    });
    
	//Accourdian
    $('.accord').click(function(){
		//$('.accord-content').removeClass('open');
        $(this).parent().siblings().removeClass('open');
        $(this).parent().toggleClass('open');

        //$('.accord-content').toggleClass('open');
    });
		//$('.accord-content').filter(':first').show();
		//$('.accord').filter(':first').find('span i.fa').addClass('fa-angle-down').removeClass('fa-angle-right');
    
    //mouse hover and mouse out
    $('.testimonial-blog').hover(
        
       function () {
           $('.testimonial-blog').removeClass('active');
          $(this).addClass('active');
       }, 
        
       function () {
          $(this).removeClass('active');
       }
    );
    
    
            
});